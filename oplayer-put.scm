(import scheme)
(import (chicken base))
(import (chicken file))
(import (chicken file posix))
(import (chicken format))
(import (chicken io))
(import (chicken pathname))
(import (chicken process-context))
(import http-client)
(import intarweb)
(import uri-common)

(define api-url "http://192.168.1.201:55555/")

(define (read-all in)
  (read-string #f in))

;; POST / HTTP/1.1
;; Host: 192.168.1.201:55555
;; Content-Type: application/x-www-form-urlencoded
;;
;; createFolder=<foldername>&ID=0&submitButton=Create

(define (create-directory! path)
  (print "Creating dir " path)
  (let ((body `((createFolder . ,path)
                (ID . "0")
                (submitButton . "Create"))))
    (call-with-input-request api-url body (lambda (in) #f))))

;; POST / HTTP/1.1
;; Host: 192.168.1.201:55555
;; X-File-Name: /<foldername>/<filename>
;; Content-Type: application/octet-stream
;;
;; <filecontent>

(define (upload-file! path full-path)
  (print "Uploading file " path)
  (let* ((dir (pathname-directory path))
         (filename (uri-encode-string (pathname-strip-directory path)))
         (header-path (make-pathname dir filename))
         (content-type 'application/content-stream)
         (req (make-request method: 'POST
                            uri: (uri-reference api-url)
                            headers: (headers
                                      `((X-File-Name ,(vector header-path 'raw))
                                        (Content-Type ,content-type)))))
        (body (call-with-input-file full-path read-all)))
    (print (call-with-input-request req body read-all))))

(define (upload-file-or-directory! parent path)
  (let ((full-path (make-pathname parent path)))
    (cond
     ((directory? full-path)
      (create-directory! path)
      (for-each (lambda (item)
                  (upload-file-or-directory! parent (make-pathname path item)))
                (directory full-path)))
     ((file-exists? full-path)
      (upload-file! path full-path)))))

(define (main music-dir)
  (when (not (and (file-exists? music-dir)
                  (directory? music-dir)))
    (fprintf (current-error-port) "No such directory: ~a\n" music-dir)
    (exit 1))
  (for-each (lambda (item)
              (print "Processing " item)
              (upload-file-or-directory! music-dir item))
            (directory music-dir)))

(apply main (command-line-arguments))
