(import scheme)
(import (chicken base))
(import (chicken port))
(import (chicken pretty-print))
(import (chicken process-context))
(import http-client)
(import medea)

(when (not (= (length (command-line-arguments)) 1))
  (with-output-to-port (current-error-port)
    (lambda () (print "Usage: " (program-name) " <message>")))
  (exit 1))

(define (getenv name)
  (or (get-environment-variable name)
      (error "Environment variable unset" name)))

(with-input-from-request "https://api.pushover.net/1/messages.json"
                         `((user . ,(getenv "PUSHOVER_USER"))
                           (token . ,(getenv "PUSHOVER_TOKEN"))
                           (message . ,(car (command-line-arguments))))
                         read-json)
