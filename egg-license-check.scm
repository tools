(import scheme)
(import (chicken base))
(import (chicken file))
(import (chicken format))
(import (chicken pathname))
(import (chicken process-context))
(import (chicken string))
(import (srfi 69))
(import medea)

(define stderr (current-error-port))
(define license-pass-count 0)
(define license-deprecated-count 0)
(define license-fail-count 0)

;; https://raw.githubusercontent.com/spdx/license-list-data/master/json/licenses.json
(define (license-list path)
  (let ((json (call-with-input-file path read-json))
        (licenses (make-hash-table)))
    (for-each (lambda (item)
                (let ((identifier (alist-ref 'licenseId item))
                      (deprecated? (alist-ref 'isDeprecatedLicenseId item)))
                  (hash-table-set! licenses identifier deprecated?)))
              (vector->list (alist-ref 'licenses json)))
    licenses))

(define (check-license path licenses)
  (let* ((filename (pathname-strip-directory path))
         (license (alist-ref 'license (call-with-input-file path read)))
         (license (and license (->string (car license))))
         (status (hash-table-ref/default licenses license 'fail)))
    (cond
     ((eqv? status 'fail)
      (set! license-fail-count (add1 license-fail-count))
      (print "[-] " filename ": " license))
     ((not status)
      (set! license-pass-count (add1 license-pass-count))
      (print "[+] " filename ": " license))
     (else
      (set! license-deprecated-count (add1 license-deprecated-count))
      (print "[?] " filename ": " license)))))

(define (check-licenses license-list-path #!optional dir)
  (let ((licenses (license-list license-list-path)))
    (find-files (or dir (current-directory))
                test: ".*\\.egg"
                action: (lambda (path _) (check-license path licenses))))
  (let ((total (+ license-pass-count license-fail-count license-deprecated-count)))
    (print license-pass-count "/" total " files use a known identifier")
    (print license-deprecated-count "/" total " files use a deprecated identifier")
    (print license-fail-count "/" total " files use an unknown identifier"))
  (when (not (zero? license-fail-count))
    (exit 1)))

(define main
  (case-lambda
   ((license-list-path)
    (check-licenses license-list-path))
   ((license-list-path dir)
    (check-licenses license-list-path dir))
   (_
    (fprintf stderr "usage: ~a <licenses.json> [directory]\n" (program-name))
    (exit 1))))

(apply main (command-line-arguments))
