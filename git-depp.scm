(import scheme)
(import (chicken base))
(import (chicken irregex))
(import (chicken pathname))
(import (chicken process))
(import (chicken process-context))
(import scsh-process)

(define (die #!rest args)
  (apply print args)
  (exit 1))

(define (trim string)
  (irregex-replace "\\s+$" string ""))

(define (main #!rest args)
  (when (not (zero? (run (git status) (> "/dev/null"))))
    (die "Not in a git repo"))
  (let* ((remote-url (trim (run/string (git remote get-url origin))))
         (match (irregex-match "git@lab:/srv/git/([^/]+\\.git)" remote-url)))
    (if match
        (let* ((repo-name (irregex-match-substring match 1))
               (remote-path (make-pathname "/srv/git" repo-name)))
          (run (ssh "git@lab" depp -C ,remote-path ,@(map qs args))))
        (die "Not in a self-hosted repo"))))

(apply main (command-line-arguments))
