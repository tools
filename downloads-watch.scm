(import scheme)
(import (chicken base))
(import (chicken file))
(import (chicken irregex))
(import (chicken pathname))
(import (chicken process-context))
(import (srfi 1))
(import (srfi 18))
(import inotify)
(import scsh-process)

(define HOME (get-environment-variable "HOME"))
;; NOTE: trailing slash
(define downloads-dir (make-pathname HOME "downloads"))
(define remote-dir "box:.")
(define rsync-flags '("--remove-source-files"
                      "-e" "ssh -l autoadd -i ~/.ssh/id_autoadd"))

(define torrent-re '(: bos (* any) ".torrent" eos))

(define timeouts '(1 1 2 3 5 8 13 21 34 55))
(define max-timeout 120)

(init!)
(on-exit clean-up!)
(add-watch! downloads-dir '(close-write moved-to))

(let loop ()
  (let* ((event (next-event!))
         (flags (event-flags event))
         (file (event-name event))
         (path (make-pathname downloads-dir file)))
    (when (and (= (length flags) 1) ; file written / moved
               (irregex-match torrent-re file))
      (let ((paths (find-files downloads-dir test: torrent-re)))
        (let loop ((timeouts timeouts))
          (for-each print paths)
          (print "uploading files...")
          (run (pipe+ ((1 2 0))
                      (rsync ,@rsync-flags ,@paths ,remote-dir)
                      (tee "/tmp/rsync.log")))
          (when (any file-exists? paths) ; rsync didn't succeed for some reason
            (let ((timeout (if (pair? timeouts) (car timeouts) max-timeout))
                  (timeouts (if (pair? timeouts) (cdr timeouts) #f)))
              (print "Backing off for " timeout " second(s)")
              (thread-sleep! timeout)
              (loop timeouts)))))))
  (loop))
